import org.testng.ITestNGListener;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;

public class Runner {

    public static void main(String[] args) {
        TestNG testNG = new TestNG();
//    testNG.addListener(new TestListenerAdapter());
        testNG.addListener((ITestNGListener) new MyCustomListener());


        final XmlSuite xmlSuite = new XmlSuite();
        xmlSuite.setName("Calculator suite");
        xmlSuite.setSuiteFiles(
                new ArrayList<String>() {{
                    add("./src/test/resources/suites/calculator.xml");
                }}
        );


        testNG.setXmlSuites(new ArrayList<XmlSuite>() {{
            add(xmlSuite);
        }});

        testNG.run();
    }
}

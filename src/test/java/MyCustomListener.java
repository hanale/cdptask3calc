import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestNGListener;

public class MyCustomListener implements ISuiteListener {

    @Override
    public void onStart(ISuite suite) {
        System.out.println("Suite started: " + suite.getName());
    }

    @Override
    public void onFinish(ISuite suite) {
    }

}

package test.calc;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class DivTest extends BaseCalculationTest {
    private long first;
    private long second;
    private long expected;

    @Factory(dataProvider = "dataForDiv")
    public DivTest(long first, long second, long expected) {
        this.first = first;
        this.second = second;
        this.expected = expected;
    }

    @DataProvider(name = "dataForDiv")
    public static Object[][] dtaForDiv() {
        return new Object[][]{
                {4, 2, 2}
        };
    }


    @Test(groups = {"long"})
    public void firstDivSecondLong() {
        long div = calculator.div(first, second);
        Assert.assertEquals(div, expected);
    }

    @Test(expectedExceptions = NumberFormatException.class, expectedExceptionsMessageRegExp = "Attempt to divide by zero", groups = {"long"})
    public void testDivOnZero() {
        calculator.div(5, 0);
    }

    @Test(expectedExceptions = NumberFormatException.class, expectedExceptionsMessageRegExp = "Attempt to divide by zero", groups = {"long"})
    public void zeroDivOnZero() {
        calculator.div(0, 0);
    }

}

package test.calc;

import com.epam.tat.module4.Timeout;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class SumTest extends BaseCalculationTest {

    private long first;
    private long second;
    private long expected;

    @Factory(dataProvider = "dataForSum")
    public SumTest(long first, long second, long expected) {
        this.first = first;
        this.second = second;
        this.expected = expected;
    }

    @DataProvider(name = "dataForSum")
    public static Object[][] dtaForSum() {
        return new Object[][]{
                {1, 2, 3}
        };
    }

    @Test(groups = {"long"})
    public void firstPlusSecondLong() {
        long sum = calculator.sum(first, second);
        Assert.assertEquals(sum, expected);
        checkTime();
        Timeout.sleep(2);
    }

    @Test(groups = {"double"})
    public void firstPlusSecond() {
        Assert.assertEquals(calculator.sum(2, 4.5), 6.5);
        checkTime();
        Timeout.sleep(2);
    }

    @Test(groups = {"long"})
    public void exceddLongBorderTest() {
        long sum = calculator.sum(9223372036854775807L, 9223372036854775806L);
        Assert.assertEquals(sum, 1.84467E+19);
        checkTime();
        Timeout.sleep(2);
    }
}

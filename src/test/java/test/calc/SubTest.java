package test.calc;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class SubTest extends BaseCalculationTest {

    private long first;
    private long second;
    private long expected;

    @Factory(dataProvider = "dataForSub")
    public SubTest(long first, long second, long expected) {
        this.first = first;
        this.second = second;
        this.expected = expected;
    }

    @DataProvider(name = "dataForSub")
    public static Object[][] dtaForSub() {
        return new Object[][]{
                {1, 2, -1},
                {2, 0, 2},
                {6, 4, 2}
        };
    }


    @Test(groups = {"long"})
    public void firstPlusSecondLong() {
        long sub = calculator.sub(first, second);
        Assert.assertEquals(sub, expected);
    }
}
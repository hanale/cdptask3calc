package test.calc;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;


public class CosTest extends BaseCalculationTest {
    private double first;
    private double expected;

    @Factory(dataProvider = "dataForCos")
    public CosTest(long first, long expected) {
        this.first = first;
        this.expected = expected;
    }

    @DataProvider(name = "dataForCos")
    public static Object[][] dataForCos() {
        return new Object[][]{
                {0, 1}
             //   {30, 0.866}
        };
    }


    @Test(groups = {"long"})
    public void cosTest() {
        double div = calculator.cos(Math.toRadians(first));
        Assert.assertEquals(div, expected);
    }
}

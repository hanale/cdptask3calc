package test.calc;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;


public class IsPositiveTest extends BaseCalculationTest {
    private long first;
    private boolean expected;

    @Factory(dataProvider = "dataForTest")
    public IsPositiveTest(long first,  boolean expected) {
        this.first = first;
        this.expected = expected;
    }

    @DataProvider(name = "dataForTest")
    public static Object[][] dataForTest() {
        return new Object[][]{
                {1, true},
                {-1, false},
                {0, false}
        };
    }


    @Test(groups = {"long"})
    public void isPositiveValue() {
        boolean isPositive = calculator.isPositive(first);
        Assert.assertEquals(isPositive, expected);
    }

}
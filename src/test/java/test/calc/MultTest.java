package test.calc;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class MultTest extends BaseCalculationTest {

    private long first;
    private long second;
    private long expected;

    @Factory(dataProvider = "dataForMult")
    public MultTest(long first, long second, long expected) {
        this.first = first;
        this.second = second;
        this.expected = expected;
    }

    @DataProvider(name = "dataForMult")
    public static Object[][] dtaForMult() {
        return new Object[][]{
                {2, 2, 4}
        };
    }


    @Test(groups = {"long"})
    public void firstMultSecondLong() {
        long mult = calculator.mult(first, second);
        Assert.assertEquals(mult, expected);
    }


    @Test(groups = {"long"})
    public void multExceddLongBorderTest() {
        double mult = calculator.mult(5.5, 4.6);
        Assert.assertEquals(mult, 25.3);
    }


}
